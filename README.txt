Koostaja: Artur Nadarjan
R�hm: 13
Kool: Eesti Infotehnoloogia Kolled�

Minu projekt on laoprogramm nimega The Warehouse. Selles programmis saab lisada, uuendada ja eemaldada asju mida kasutaja soovib lisada. 
Lisaks on ka sisse logimine, registreerimine ja v�lja logimine. Projekt koosneb seitsmest failidest. Kuus on PHP failid ja �ks on JavaScript fail.

View_register.php, view_login.php ja view.php on programmi disain failid. Need on kasutatud, et saavutada v�limuse.

Ladu.js on kasutatud, et kontrollida vormis olevaid v��rtusi ja lisada tabelisse uued read, kontrollib v��rtused mida lisatakse.

Ladu.php on kasutatud, et saavutada sessiooni, andmete modifitseerimine ja haldamine.

Controller.php on mis paneb programmi t��le ehk see lubab kasutajal registreerida, sisse logida, v�lja logida ja teavitada kasutajale kui nende sisse logimine eba�nnestus v�i kui eba�nnestus uuendada tabelit.

Model.php on kasutatud, et programm t��taks andmebaasiga.

Projektis k�ige raskem osa oli controlleri & modeli koostamine ja koodi korrektse v�limuse s�ilitamine. 
Projekt on minu arvates rahuldav.
