<!doctype HTML>
<html>

<head>
    <title>The Warehouse</title>
    <meta charset="utf-8">

    <style>
        #lisa-vorm {
            display: none;
        }
        
        body {
	    background: url('https://s-media-cache-ak0.pinimg.com/736x/d0/ee/89/d0ee8955ee5b5f5ebe2f818e64177495.jpg') center fixed;
	    color: black;
	    margin:55px;
        }        
    </style>

</head>

<body>

    <?php foreach (message_list() as $message):?>
        <p style="border: 1px solid black; background: #dddddd; ">
            <?= $message; ?>
        </p>
    <?php endforeach; ?>

    <div style="float: right;">
        <form method="post"  action="<?= $_SERVER['PHP_SELF']; ?>">
            <input type="hidden" name="action" value="logout">
            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
            <button type="submit">Sign out</button>
        </form>
    </div>

    <h1>Storage</h1>
    <h2>Welcome to The Warehouse! You can add, update or remove items</h2>
    
    <p id="kuva-nupp">
        <button type="button">Add more items</button>
    </p>

    <form id="lisa-vorm" method="post" action="<?= $_SERVER['PHP_SELF']; ?>">

        <input type="hidden" name="action" value="add">
        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">

        <p id="peida-nupp">
            <button type="button">Finished</button>
        </p>

        <table>
            <tr>
                <td>Name of the item</td>
                <td>
                    <input type="text" id="nimetus" name="nimetus">
                </td>
            </tr>
            <tr>
                <td>Amount</td>
                <td>
                    <input type="number" id="kogus" name="kogus">
                </td>
            </tr>
        </table>

        <p>
            <button type="submit">Sumbit</button>
        </p>

    </form>

    <table id="ladu" border="1">
        <thead>
            <tr>
                <th>Items</th>
                <th>Amount</th>
                <th>Activities</th>
            </tr>
        </thead>

        <tbody>

        <?php
        //koolon tsükli lõpus tähendab, et tsükkel koosneb HTML koodist
        foreach (model_load($page) as $rida): ?>

            <tr>
                <td>
                    <?=
                        // et vältida pahatahtlikku XSS sisu, kus kasutaja sisestab õige
                        // info asemel <script> tag'i, peame tekstiväljundis asendama kõik HTML erisümbolid
                        htmlspecialchars($rida['nimetus']);
                    ?>
                </td>
                <td>
                    <form method="post" action="<?= $_SERVER['PHP_SELF'];?>">
                        <input type="hidden" name="action" value="update">
                        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token'];?>">
                        <input type="hidden" name="id" value="<?= $rida['id'];?>">

                        <input type="number" style="width: 5em; text-align: right;" name="kogus" value="<?= $rida['kogus']; ?>">
                        <button type="submit">Update</button>
                    </form>
                </td>
                <td>

                    <form method="post" action="<?= $_SERVER['PHP_SELF'];?>">
                        <input type="hidden" name="action" value="delete">
                        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                        <input type="hidden" name="id" value="<?= $rida['id']; ?>">
                        <button type="submit">Delete row</button>
                    </form>

                </td>
            </tr>

        <?php endforeach; ?>

        </tbody>
    </table>

    <script src="ladu.js"></script>
</body>

</html>