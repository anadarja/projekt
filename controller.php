<?php
// Lisab uued kirjad
function controller_add($nimetus, $kogus)
{
    if (!controller_user()) {
        message_add('Tegevus eeldab sisselogimist');
        return false;
    }
    // kontrollime kas sisendväärtused on oodatud kujul või mitte
    if ($nimetus == '' || $kogus <= 0) {
        message_add('Invalid input');
        return false;
    }
    if (model_add($nimetus, $kogus)) {
        message_add('New items have been added!');
        return true;
    }
    message_add('Failed to add items');
    return false;
}
// Kustutab kirje
function controller_delete($id)
{
    if (!controller_user()) {
        message_add('You are required to login');
        return false;
    }
    if ($id <= 0) {
        message_add('Invalid input');
        return false;
    }
    if (model_delete($id)) {
        message_add('Removed row '.$id);
        return true;
    }
    message_add('Failed to remove!');
    return false;
}
// Uuendab kirje väärtust
function controller_update($id, $kogus)
{
    if (!controller_user()) {
        message_add('You are required to login');
        return false;
    }
    if ($id <= 0 || $kogus <= 0) {
        message_add('Invalid input');
        return false;
    }
    if (model_update($id, $kogus)) {
        message_add('Updated row '.$id);
        return true;
    }
    message_add('Failed to update your items!');
    return false;
}
// Kontrollib kas kasutaja on sisse loginud voi mitte
function controller_user()
{
    if (empty($_SESSION['login'])) {
        return false;
    }
    return $_SESSION['login'];
}
// Lisab uue kasutaja
function controller_register($kasutajanimi, $parool)
{
    if ($kasutajanimi == '' || $parool == '') {
        message_add('Invalid input');
        return false;
    }
    if (model_user_add($kasutajanimi, $parool)) {
        message_add('The account is registered');
        return true;
    }
    message_add('Account registration failed, the username may have been taken');
    return false;
}
// Logib kasutaja sisse
function controller_login($kasutajanimi, $parool)
{
    if ($kasutajanimi == '' || $parool == '') {
        message_add('Invalid input');
        return false;
    }
    $id = model_user_get($kasutajanimi, $parool);
    if (!$id) {
        message_add('Invalid username or password');
        return false;
    }
    session_regenerate_id();
    $_SESSION['login'] = $id;
    message_add('Welcome! You are logged in');
    return $id;
}
// Logib kasutaja välja
function controller_logout()
{
    // muuda sessiooni ku?psis kehtetuks
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time() - 42000, '/');
    }
    // tühjendab sessiooni massiiv
    $_SESSION = array();
    // lõpeta sessioon
    session_destroy();
    message_add('You have signed out!');
    return true;
}
// Lisab järjekorda uue sõnumi kasutajale kuvamiseks
function message_add($message)
{
    if (empty($_SESSION['messages'])) {
        $_SESSION['messages'] = array();
    }
    $_SESSION['messages'][] = $message;
}
// Tagastab kõik hetkel ootel olevad sõnumid
function message_list()
{
    if (empty($_SESSION['messages'])) {
        return array();
    }
    $messages = $_SESSION['messages'];
    $_SESSION['messages'] = array();
    return $messages;
}