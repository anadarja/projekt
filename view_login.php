<!doctype html>
<html>
    <head>
        <meta charset="utf8" />
        <title>Login</title>
    </head>
    
    <style>
        body {
	    background: url('https://s-media-cache-ak0.pinimg.com/736x/d0/ee/89/d0ee8955ee5b5f5ebe2f818e64177495.jpg') center fixed;
	    color: black;
        }
    </style>
    
    <body>

        <?php foreach (message_list() as $message):?>
            <p style="border: 1px solid black; background: rgba(255, 0, 0, 0.26);">
                <?= $message; ?>
            </p>
        <?php endforeach; ?>

        <h1>Login</h1>
        <h2>Welcome to The Warehouse! Please login to proceed</h2>

        <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">

            <input type="hidden" name="action" value="login">
            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">

            <table>
                <tr>
                    <td>Username</td>
                    <td>
                        <input type="text" name="kasutajanimi" required>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="parool" required>
                    </td>
                </tr>
            </table>

            <p>
                <button type="submit">Login</button> or <a href="<?= $_SERVER['PHP_SELF']; ?>?view=register">register an account</a>
            </p>

        </form>
    </body>
</html>